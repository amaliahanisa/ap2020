package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;
    private Member master;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
        master = new PremiumMember("Heath", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName()) ;
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole()) ;

    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new PremiumMember("child", "child");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new PremiumMember("child", "child");
        member.addChildMember(child);
        assertEquals(1, member.getChildMembers().size());
        member.removeChildMember(child);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child = new PremiumMember("child", "premium");
        member.addChildMember(child);
        Member child1 = new PremiumMember("child", "premium");
        member.addChildMember(child1);
        Member child2 = new PremiumMember("child", "premium");
        member.addChildMember(child2);
        Member child3 = new PremiumMember("child", "premium");
        member.addChildMember(child3);
        Member child4 = new PremiumMember("child", "premium");
        member.addChildMember(child4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child = new PremiumMember("child", "premium");
        master.addChildMember(child);
        Member child1 = new PremiumMember("child", "premium");
        master.addChildMember(child1);
        Member child2 = new PremiumMember("child", "premium");
        master.addChildMember(child2);
        Member child3 = new PremiumMember("child", "premium");
        master.addChildMember(child3);
        assertEquals(4 , master.getChildMembers().size());

    }
}
