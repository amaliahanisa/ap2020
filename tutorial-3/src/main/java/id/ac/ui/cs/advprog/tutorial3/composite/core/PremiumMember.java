package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    public String name;
    public String role;
    public List<Member> childList;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
        this.childList = new ArrayList<>();
    }
    public String getName(){
        return this.name;
    }
    public String getRole(){
        return this.role;
    }
    public void addChildMember(Member member){
        if(this.role.equals("Master")){
            childList.add(member);
        }
        else if(childList.size() < 3){
            childList.add(member);
        }
    }
    public void removeChildMember(Member member){
        if(childList.size() > 0){
            childList.remove(member);
        }
    }
    public List<Member> getChildMembers(){
        return childList;
    }
}
