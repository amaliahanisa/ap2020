package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    public String name;
    public String role;
    public List<Member> memberList;

    public OrdinaryMember(String name,String role){
        this.name = name;
        this.role = role;
        memberList = new ArrayList<>();
    }
    public String getName(){
        return this.name;
    }
    public String getRole(){
        return this.role;
    }
    public void addChildMember(Member member){}
    public void removeChildMember(Member member){}
    public List<Member> getChildMembers(){
        return memberList;
    }
}
